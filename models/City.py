import json
from random import randint

from faker import Faker
import endpoints
import requests

fake = Faker()


class City:
    def __init__(self):
        self.createdAt = None
        self.id = None
        self.name = fake.city()
        self.population = randint(10000, 600000)

    def create(self):
        body = {
            "name": self.name,
            "population": self.population
        }
        create_city_response = requests.post(endpoints.base_url, json=body)

        if create_city_response.status_code == 201:
            self.id = create_city_response.json()["id"]
            self.createdAt = create_city_response.json()["createdAt"]

        assert create_city_response.status_code == 201

    def update(self, name, population):
        body = {
            "name": name,
            "population": population
        }
        create_city_response = requests.put("{0}/{1}".format(endpoints.base_url, self.id), json=body)
        assert create_city_response.status_code == 200

    def delete(self):
        if self.id is not None:
            delete_city_response = requests.delete("{0}/{1}".format(endpoints.base_url, self.id))
            assert delete_city_response.status_code == 204


if __name__ == '__main__':
    city = City()
    print(city.name)
    print(city.population)

    city.create()
    city.update(fake.city(), randint(10000, 600000))
    city.delete()