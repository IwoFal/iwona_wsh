from random import randint

import pytest
import requests
from faker import Faker
import endpoints
from models.City import City

fake = Faker()


def test_get_city_list(new_city):
    response = requests.get(endpoints.base_url)
    name_list = [a['name'] for a in response.json()]
    assert new_city.name in name_list


def test_get_city(new_city):
    response = requests.get("{0}/{1}".format(endpoints.base_url, new_city.id))
    assert response.json()['name'] == new_city.name and response.json()['population'] == new_city.population


def test_delete_city(new_city):
    new_city.delete()
    response = requests.get("{0}/{1}".format(endpoints.base_url, new_city.id))
    assert response.status_code == 404


def test_update_city(new_city):
    new_name = fake.city()
    new_population = randint(10000, 600000)

    new_city.update(new_name, new_population)

    response = requests.get("{0}/{1}".format(endpoints.base_url, new_city.id))

    assert response.json()['name'] == new_name and response.json()['population'] == new_population


@pytest.fixture
def new_city():
    city = City()
    city.create()
    return city
